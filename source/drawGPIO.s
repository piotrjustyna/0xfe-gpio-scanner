.section .text

    gpio:           .word 0x20200000
    gpioPinLevels:  .word 0x20200034
    whiteColour:    .word 0xFFFF

    .globl DrawGPIOFunctions
    DrawGPIOFunctions:   PUSH { lr }

        x .req r4
        y .req r5
        registerBuffer .req r6
        testedNumber .req r7
        pinCounter .req r8
        gpioAddress .req r9

        LDR gpioAddress, gpio
        LDR registerBuffer, [gpioAddress]
        MOV testedNumber, #(1 << 2)

        // GROUP 1
            // PIN 0
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 0

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 1
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 1

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 2
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 2

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 3
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 3

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 4
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 4

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 2
            // PIN 5
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 5

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 6
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 6

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 7
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 7

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 8
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 8

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 9
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 9

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 3
            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 10
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 10

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 11
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 11

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 12
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 12

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 13
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 13

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 14
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 14

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 4
            // PIN 15
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 15

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 16
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 16

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 17
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 17

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 18
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 18

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 19
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 19

            ADD y, #12
            SUB x, #(2 * 5)
            MOV testedNumber, #(1 << 2)

        // GROUP 5
            LDR registerBuffer, [gpioAddress, #0x8]

            // PIN 20
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 20

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 21
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 21

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 22
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 22

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 23
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 23

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 24
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 24

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 6
            // PIN 25
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 25

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 26
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 26

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 27
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 27

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 28
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 28

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 29
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 29

            ADD y, #12
            SUB x, #(2 * 5)
            MOV testedNumber, #(1 << 2)

        // GROUP 7
            LDR registerBuffer, [gpioAddress, #0xC]

            // PIN 30
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 30

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 31
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 31

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 32
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 32

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 33
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 33

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 34
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 34

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 8
            // PIN 35
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 35

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 36
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 36

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 37
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 37

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 38
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 38

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 39
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 39

            ADD y, #12
            SUB x, #(2 * 5)
            MOV testedNumber, #(1 << 2)

        // GROUP 9
            LDR registerBuffer, [gpioAddress, #0x10]

            // PIN 40
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 40

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 41
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 41

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 42
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 42

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 43
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 43

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 44
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 44

            ADD y, #12
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

        // GROUP 10
            // PIN 45
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 45

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 46
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 46

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 47
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 47

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 48
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 48

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 49
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 49

            ADD y, #12
            SUB x, #(2 * 5)
            MOV testedNumber, #(1 << 2)

        // GROUP 11
            LDR registerBuffer, [gpioAddress, #0x14]

            // PIN 50
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 50

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 51
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 51

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 52
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 52

            ADD y, #6
            SUB x, #(2 * 5)
            LSR registerBuffer, #3
            MOV testedNumber, #(1 << 2)

            // PIN 53
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD x, #5
            LSR testedNumber, #1

            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            // <- PIN 53

            ADD x, #(2 * 5)
            SUB y, #(6 * 64) // 6 * (54 + 10)
            ADD y, #6

        .unreq registerBuffer
        .unreq testedNumber
        .unreq gpioAddress
        .unreq x
        .unreq y
        .unreq pinCounter

        POP { pc }

     .globl DrawGPIOPinLevels
     DrawGPIOPinLevels:  PUSH { lr }

        x .req r4
        y .req r5
        registerBuffer .req r6
        testedNumber .req r7
        pinCounter .req r8
        gpioAddress .req r9

        LDR gpioAddress, gpioPinLevels
        LDR registerBuffer, [gpioAddress]
        MOV testedNumber, #1

        // GROUP 1
            // PIN 0
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 0

            // PIN 1
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 1

            // PIN 2
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 2

            // PIN 3
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 3

            // PIN 4
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 4

        // GROUP 2
            // PIN 5
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 5

            // PIN 6
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 6

            // PIN 7
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 7

            // PIN 8
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 8

            // PIN 9
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 9

        // GROUP 3
            // PIN 10
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 10

            // PIN 11
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 11

            // PIN 12
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 12

            // PIN 13
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 13

            // PIN 14
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 14

        // GROUP 4
            // PIN 15
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 15

            // PIN 16
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 16

            // PIN 17
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 17

            // PIN 18
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 18

            // PIN 19
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 19

        // GROUP 5
            // PIN 20
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 20

            // PIN 21
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 21

            // PIN 22
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 22

            // PIN 23
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 23

            // PIN 24
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 24

        // GROUP 6
            // PIN 25
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 25

            // PIN 26
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 26

            // PIN 27
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 27

            // PIN 28
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 28

            // PIN 29
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 29

        // GROUP 7
            // PIN 30
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 30

            // PIN 31
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 31

            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 32
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 32

            // PIN 33
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 33

            // PIN 34
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 34

        // GROUP 8
            // PIN 35
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 35

            // PIN 36
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 36

            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 37
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 37

            // PIN 38
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 38

            // PIN 39
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 39

        // GROUP 9
            // PIN 40
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 40

            // PIN 41
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 41

            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 42
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 42

            // PIN 43
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 43

            // PIN 44
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 44

        // GROUP 10
            // PIN 45
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 45

            // PIN 46
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 46

            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 47
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 47

            // PIN 48
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 48

            // PIN 49
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #12
            LSR registerBuffer, #1
            // <- PIN 49

        // GROUP 10
            // PIN 50
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 50

            // PIN 51
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 51

            LDR registerBuffer, [gpioAddress, #0x4]

            // PIN 52
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 52

            // PIN 53
            TST registerBuffer, testedNumber
            BLEQ Draw0
            BLNE Draw1
            ADD y, #6
            LSR registerBuffer, #1
            // <- PIN 53

        SUB y, #384     // (6 * 6 * 10) + (6 * 4)

        .unreq registerBuffer
        .unreq testedNumber
        .unreq gpioAddress
        .unreq x
        .unreq y
        .unreq pinCounter

        POP { pc }

    .globl SetGPIO
    SetGPIO:    PUSH { lr }

                gpioAddress .req r6
                actLed .req r7

                LDR gpioAddress, =0x20200000

                LDR actLed, =(0b1 << 21)
                STR actLed, [gpioAddress]
                
                LDR actLed, =(1 << 7)
                STR actLed, [gpioAddress, #0x1C]

                .unreq gpioAddress
                .unreq actLed

                POP { pc }

    DrawPixel:  PUSH { lr } 

                frameBufferInfo .req r0
                framebufferAddress .req r1
                screenWidth .req r2
                colour .req r3
                x .req r4
                y .req r5
                pixelAddress .req r6

                MLA pixelAddress, screenWidth, y, x
                ADD pixelAddress, pixelAddress
                ADD pixelAddress, framebufferAddress

                STRH colour, [pixelAddress]

                .unreq frameBufferInfo
                .unreq framebufferAddress
                .unreq screenWidth
                .unreq colour
                .unreq x
                .unreq y
                .unreq pixelAddress

                POP { pc }

    .globl Draw1
    Draw1:  PUSH { r6, lr }

            colour .req r3
            x .req r4
            y .req r5

            /* Row 1 */
                ADD x, #3
                BL DrawPixel
            /* <- Row 1 */

            /* Row 2 */
                SUB y, #1
                BL DrawPixel
            /* <- Row 2 */

            /* Row 3 */
                SUB y, #1
                BL DrawPixel
            /* <- Row 3 */

            /* Row 4 */
                SUB y, #1

                SUB x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 4 */

            /* Row 5 */
                SUB y, #1
                BL DrawPixel
            /* <- Row 5 */

            MOV colour, #0
            SUB x, #3
            ADD y, #4

            /* Row 1 */
                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 1 */

            /* Row 2 */
                SUB y, #1

                BL DrawPixel

                SUB x, #1
                BL DrawPixel

                SUB x, #1
                BL DrawPixel
            /* <- Row 2 */

            /* Row 3 */
                SUB y, #1

                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 3 */

            /* Row 4 */
                SUB y, #1
                SUB x, #1

                BL DrawPixel

                SUB x, #1
                BL DrawPixel
            /* <- Row 4 */

            /* Row 5 */
                SUB y, #1

                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 4 */

            LDR colour, =0xFFFF
            SUB x, #2
            ADD y, #4

            .unreq x
            .unreq y
            .unreq colour

            POP { r6, pc }

    .globl Draw0
    Draw0:  PUSH { r6, lr }

            colour .req r3
            x .req r4
            y .req r5

            /* Row 1 */
                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 1 */

            /* Row 2 */
                SUB y, #1

                BL DrawPixel

                SUB x, #3
                BL DrawPixel
            /* <- Row 2 */

            /* Row 3 */
                SUB y, #1

                BL DrawPixel

                ADD x, #3
                BL DrawPixel
            /* <- Row 3 */

            /* Row 4 */
                SUB y, #1

                BL DrawPixel

                SUB x, #3
                BL DrawPixel
            /* <- Row 4 */

            /* Row 5 */
                SUB y, #1

                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 5 */

            MOV colour, #0
            SUB x, #2
            ADD y, #3

            /* Row 2 */
                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 2 */

            /* Row 3 */
                SUB y, #1

                BL DrawPixel

                SUB x, #1
                BL DrawPixel
            /* <- Row 3 */

            /* Row 4 */
                SUB y, #1

                BL DrawPixel

                ADD x, #1
                BL DrawPixel
            /* <- Row 4 */

            LDR colour, =0xFFFF
            SUB x, #2
            ADD y, #3

            .unreq x
            .unreq y
            .unreq colour

            POP { r6, pc }
