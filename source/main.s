.section .init

    .globl _start
    _start: MOV sp, #0x8000
            B main

.section .text

    main:   BL InitialiseFrameBuffer
            frameBufferInfo .req r0
            TEQ frameBufferInfo, #0
            BEQ finishingLoop

            framebufferAddress .req r1
            LDR framebufferAddress, [frameBufferInfo, #32]

            screenWidth .req r2
            LDR screenWidth, [frameBufferInfo, #0]

            colour .req r3
            LDR colour, =0xFFFF

            BL SetGPIO

            x .req r4
            y .req r5
            LDR x, =0
            LDR y, =11

            .unreq frameBufferInfo
            .unreq framebufferAddress
            .unreq colour

            initialTimestamp .req r10

            BL DrawGPIOFunctions

            finishingLoop:

                LDR initialTimestamp, =0x20003004
                LDR initialTimestamp, [initialTimestamp]

                BL DrawGPIOPinLevels
                SUB y, #6
                SUB x, #(5 * 4)
                BL DrawTimeDifference
                ADD y, #6
                ADD x, #(5 * 4)

            B finishingLoop

DrawTimeDifference: PUSH { lr }

                x .req r4
                y .req r5
                testedNumber .req r6
                previousValue .req r10
                testedRegister .req r11

                MOV testedNumber, #1
                LSL testedNumber, #31
                LDR testedRegister, =0x20003004
                LDR testedRegister, [testedRegister]
                SUB testedRegister, testedRegister, previousValue

                testLoop1:

                    TST testedRegister, testedNumber
                    BLEQ Draw0
                    BLNE Draw1
                    ADD x, #5

                    LSR testedNumber, #1
                    TEQ testedNumber, #0

                BNE testLoop1

                SUB x, #(32 * 5)


                .unreq x
                .unreq y
                .unreq testedNumber
                .unreq previousValue
                .unreq testedRegister

                POP { pc }
